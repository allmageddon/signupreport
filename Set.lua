Set = {}
Set.mt = {} -- metatable for sets
function Set.new(t)
    local set = {}
    setmetatable(set, Set.mt)
    for _, l in ipairs(t) do
        set[l] = true
    end
    return set
end

function Set:count()
    return Set.count(self)
end
function Set.count(set)
    local count = 0
    for k, v in pairs(set) do
        count = count + 1
    end
    return count
end

function Set.union(a, b)
    local res = Set.new {}
    for k in pairs(a) do res[k] = true end
    for k in pairs(b) do res[k] = true end
    return res
end

function Set.intersection(a, b)
    local res = Set.new {}
    for k in pairs(a) do
        res[k] = b[k]
    end
    return res
end

function Set.difference(a, b)
    local res = Set.new {}
    for k in pairs(a) do
        if b[k] == nil
        then res[k] = true
        end
    end
    return res
end

function Set.basenames(a)
    local basename = function(str) return string.gsub(str, "%-.+", "") end
    return Set.transform(a, basename)
end

function Set.contains(s, item)
    if s[item] == true then
        return true
    end
    return false
end

function Set.transform(a, transformation)
    local res = Set.new {}
    for k in pairs(a) do
        local newkey = transformation(k)
        res[newkey] = true
    end
    return res
end

function Set.join(l, sep)
    local s = ""
    local res = {}
    for k in pairs(l) do
        res[#res + 1] = k
    end
    local s = str_join(sep, res)
    return s
end

Set.mt.__add = Set.union
Set.mt.__mul = Set.intersection
Set.mt.__sub = Set.difference
Set.mt.__le = function(a, b) -- set containment
for k in pairs(a) do
    if not b[k] then return false end
end
return true
end
Set.mt.__lt = function(a, b)
    return a <= b and not (b <= a)
end
Set.mt.__eq = function(a, b)
    return a <= b and b <= a
end
Set.mt.__tostring = function(tbl) return Set.join(tbl, ", ") end
Set.mt.__concat = function(a, b) return tostring(a) .. tostring(b) end
Set.mt.__len = Set.count

