function GetAllSignups()
    local numSigned = CalendarEventGetNumInvites();
    if numSigned == 0 then
        error("No calendar event selected.")
    end
    local signed = {}
    for i = 1, numSigned do
        local raider = CalendarEventGetInvite(i)
        signed[i] = raider
    end
    return signed
end



function GetAllRaidMembers()
    local names = {}
    local numRaiding = GetNumGroupMembers()
    if numRaiding == 0 then
        error("Make sure to be in a raid before generating a report.")
    end
    for i = 1, numRaiding do
        local name, rank, subgroup, level, class, fileName, zone, online, isDead, role, isML = GetRaidRosterInfo(i);
        names[i] = name
    end
    return names
end

function AltToMainMap()
    local guildees = Set.basenames(Set.new(GetAllGuildMembers()))
    local lookup = {}
    for i = 1, GetNumGuildMembers() do
        name, rank, rankIndex, level, class, zone, note,
        officernote, online, status, classFileName,
        achievementPoints, achievementRank, isMobile = GetGuildRosterInfo(i);
        mainname = officernote
        if Set.contains(guildees, mainname) then
            lookup[basename(name)] = mainname
        end
    end
    return lookup
end

function Set.tomains(s)
    local lookup = AltToMainMap()
    local basenames = Set.basenames(s)
    local result = {}
    for k, v in pairs(basenames) do
        result[lookup[k] or k] = true
    end
    return result
end

function GetAllGuildMembers()
    local names = {}
    local numGuild = GetNumGuildMembers()
    if numGuild == 0 then
        error("You are not in a guild.")
    end
    for i = 1, numGuild do
        name, rank, rankIndex, level, class, zone, note,
        officernote, online, status, classFileName,
        achievementPoints, achievementRank, isMobile = GetGuildRosterInfo(i);
        names[i] = name
    end
    return names
end

function report(format)
    local signed = GetAllSignups()
    signed = Set.new(signed)
    local inRaid = GetAllRaidMembers()
    inRaid = Set.basenames(Set.new(inRaid))
    local inGuild = GetAllGuildMembers()
    inGuild = Set.basenames(Set.new(inGuild))
    signed = Set.tomains(signed)
    inRaid = Set.tomains(inRaid)
    guildeesInRaid = inGuild * inRaid
    guildeesSigned = inGuild * signed
    signedInRaid = guildeesSigned * guildeesInRaid
    signedNotInRaid = guildeesSigned - guildeesInRaid
    notSignedInRaid = guildeesInRaid - guildeesSigned
    return format(signedInRaid, signedNotInRaid, notSignedInRaid)
end

function format_export_gdocs(signedInRaid, signedNotInRaid, notSignedInRaid)
    s = "SIGNUPREPORT"
    s = s .. "-_-_-_" .. Set.join(signedInRaid, ",")
    s = s .. "-_-_-_" .. Set.join(signedNotInRaid, ",")
    s = s .. "-_-_-_" .. Set.join(notSignedInRaid, ",")
    return s
end

function format_export(signedInRaid, signedNotInRaid, notSignedInRaid)
    s = ""
    if Set.count(signedInRaid) > 0 then
        s = s .. Set.join(signedInRaid, "\n") .. "\n"
    end
    if Set.count(signedNotInRaid) > 0 then
        s = s .. Set.join(signedNotInRaid, "\n") .. "\n"
    end
    if Set.count(notSignedInRaid) > 0 then
        s = s .. Set.join(notSignedInRaid, "\n")
    end
    return s
end

function format_chat(signedInRaid, signedNotInRaid, notSignedInRaid)
    s = "Signup report: \n"
    if Set.count(signedInRaid) > 0 then
        s = s .. "Tryhards - Signed and present: "
        s = s .. tostring(signedInRaid) .. "\n\n"
    end
    if Set.count(signedNotInRaid) > 0 then
        s = s .. "Time Slackers - Signed but NOT present: "
        s = s .. tostring(signedNotInRaid) .. "\n\n"
    end
    if Set.count(notSignedInRaid) > 0 then
        s = s .. "Signup Slackers - NOT signed but present: "
        s = s .. tostring(notSignedInRaid) .. "\n\n"
    end
    return s
end

function export_handler()
    local ok, result = pcall(report, format_export_gdocs)
    if not ok then
        print("Error: " .. result)
    end
    export_popup(result)
end

function export_popup(str)
    local CopyChat = CreateFrame('Frame', 'nChatCopy', UIParent)
    CopyChat:SetWidth(500)
    CopyChat:SetHeight(400)
    CopyChat:SetPoint('LEFT', UIParent, 'LEFT', 3, 10)
    CopyChat:SetFrameStrata('DIALOG')
    CopyChat:Hide()
    CopyChat:SetBackdrop({
        bgFile = [[Interface\Buttons\WHITE8x8]],
        insets = {
            left = 3,
            right = 3,
            top = 4,
            bottom = 3
        }
    })
    CopyChat:SetBackdropColor(0, 0, 0, 0.7)

    CopyChatBox = CreateFrame('EditBox', 'nChatCopyBox', CopyChat)
    CopyChatBox:SetMultiLine(true)
    CopyChatBox:SetAutoFocus(true)
    CopyChatBox:EnableMouse(true)
    CopyChatBox:SetMaxLetters(99999)
    CopyChatBox:SetFont('Fonts\\ARIALN.ttf', 13, 'THINOUTLINE')
    CopyChatBox:SetText(str)
    CopyChatBox:SetWidth(590)
    CopyChatBox:SetHeight(590)
    CopyChatBox:HighlightText(0, -1)
    CopyChatBox:SetScript('OnEscapePressed', function() CopyChat:Hide() end)

    local Scroll = CreateFrame('ScrollFrame', 'nChatCopyScroll', CopyChat, 'UIPanelScrollFrameTemplate')
    Scroll:SetPoint('TOPLEFT', CopyChat, 'TOPLEFT', 8, -30)
    Scroll:SetPoint('BOTTOMRIGHT', CopyChat, 'BOTTOMRIGHT', -30, 8)
    Scroll:SetScrollChild(CopyChatBox)
    CopyChat:Show()
end
