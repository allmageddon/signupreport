package.path = "../?.lua;" .. package.path
require("Set")
require("Util")

function fix(set)
    local res = {}
    for k in pairs(set) do
        res[#res + 1] = k;
    end
    table.sort(res)
    return res
end

describe("Set logic", function()
    describe("Union", function()
        it("Combines all uniques in two sets", function()
            assert.are.same(fix(Set.new({ "Shallow", "Shadybreh", "Greyhat", "Sturgeon" })),
                fix(Set.new { "Greyhat", "Sturgeon" } + Set.new { "Shallow", "Shadybreh" }))
        end)
    end)
    describe("Contains", function()
        it("Finds items in the set", function()
            assert.is_true(Set.new { "Greyhat", "Rauðhattur" }["Greyhat"])
        end)
        it("Finds unicode items in the set", function()
            assert.is_true(Set.new { "Greyhat", "Rauðhattur" }["Rauðhattur"])
        end)
    end)
    describe("Basenames", function()
        it("Removes server names from each element in a set", function()
            assert.are.same(fix(Set.new({ "Greyhat", "Rauðhattur" })),
                fix(Set.basenames(Set.new { "Greyhat-Ragnaros", "Rauðhattur-Draenor" })))
        end)
    end)
    describe("Difference", function()
        it("Finds items in the first set, but not in the second.", function()
            assert.are.same(fix(Set.new({ "Greyhat" })),
                fix(Set.new { "Greyhat", "Sturgeon" } - Set.new { "Sturgeon", "Shadybreh" }))
        end)
    end)
    describe("Intersection", function()
        it("Finds common items in two sets", function()
            assert.are.same(fix(Set.new({ "Greyhat" })),
                fix(Set.new { "Greyhat", "Sturgeon" } * Set.new { "Greyhat", "Shadybreh" }))
        end)
    end)
    describe("Count", function()
        it("Returns the correct size of a set", function()
            assert.are.same(0, Set.count(Set.new {}))
            assert.are.same(1, Set.count(Set.new { "Greyhat" }))
            assert.are.same(2, Set.count(Set.new { "Greyhat", "Sturgeon" }))
        end)
    end)
end)

