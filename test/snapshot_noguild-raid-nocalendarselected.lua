return {
    ["GetRaidRosterInfo"] = {
        ["{\n 1 = 2,\n}\n"] = {
            "Stillbit", -- [1]
            0, -- [2]
            1, -- [3]
            23, -- [4]
            "Warlock", -- [5]
            "WARLOCK", -- [6]
            "Forsaken Rear Guard", -- [7]
            true, -- [8]
            false, -- [9]
            nil, -- [10]
            false, -- [11]
            "DAMAGER", -- [12]
        },
        ["{\n 1 = 3,\n}\n"] = {
            nil, -- [1]
            0, -- [2]
            1, -- [3]
            1, -- [4]
            nil, -- [5]
            nil, -- [6]
            nil, -- [7]
            nil, -- [8]
            nil, -- [9]
            nil, -- [10]
            false, -- [11]
            "NONE", -- [12]
        },
        ["{\n 1 = 1,\n}\n"] = {
            "Bithat", -- [1]
            2, -- [2]
            1, -- [3]
            94, -- [4]
            "Monk", -- [5]
            "MONK", -- [6]
            "Frostwall", -- [7]
            true, -- [8]
            false, -- [9]
            nil, -- [10]
            false, -- [11]
            "DAMAGER", -- [12]
        },
    },
    ["CalendarEventGetNumInvites"] = {
        ["{\n}\n"] = {
            0, -- [1]
            false, -- [2]
        },
    },
    ["GetNumGroupMembers"] = {
        ["{\n}\n"] = {
            2, -- [1]
        },
    },
    ["CalendarEventGetInvite"] = {
        ["{\n 1 = 1,\n}\n"] = {
            nil, -- [1]
            0, -- [2]
            [7] = false,
            [8] = 0,
        },
    },
    ["GetNumGuildMembers"] = {
        ["{\n}\n"] = {
            0, -- [1]
            0, -- [2]
            0, -- [3]
        },
    },
    ["GetGuildRosterInfo"] = {
        ["{\n 1 = 1,\n}\n"] = {
            nil, -- [1]
            nil, -- [2]
            0, -- [3]
            0, -- [4]
            nil, -- [5]
            nil, -- [6]
            nil, -- [7]
            nil, -- [8]
            nil, -- [9]
            "", -- [10]
            nil, -- [11]
            0, -- [12]
            0, -- [13]
            false, -- [14]
            false, -- [15]
            0, -- [16]
        },
    },
}
