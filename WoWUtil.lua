function chatLines(s, channel)
    for i, v in ipairs(str_split("\n", s)) do
        SendChatMessage(v, channel, nil, nil)
    end
end

function basename(char_name)
    -- Removes the realm name suffix if one exists
    return string.gsub(char_name, "%-.+", "")
end

