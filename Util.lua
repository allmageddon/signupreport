-- Concat the contents of the parameter list,
-- separated by the string delimiter (just like in perl)
-- example: strjoin(", ", {"Anna", "Bob", "Charlie", "Dolores"})
function str_join(delimiter, list)
    local len = #list
    if len == 0 then
        return ""
    end
    local string = list[1]
    for i = 2, len do
        string = string .. delimiter .. list[i]
    end
    return string
end

-- Split text into a list consisting of the strings in text,
-- separated by strings matching delimiter (which may be a pattern).
-- example: strsplit(",%s*", "Anna, Bob, Charlie,Dolores")
function str_split(delimiter, text)
    local list = {}
    local pos = 1
    if strfind("", delimiter, 1) then -- this would result in endless loops
    error("delimiter matches empty string!")
    end
    while 1 do
        local first, last = strfind(text, delimiter, pos)
        if first then -- found?
        tinsert(list, strsub(text, pos, first - 1))
        pos = last + 1
        else
            tinsert(list, strsub(text, pos))
            break
        end
    end
    return list
end


function printList(l)
    for _, entry in pairs(l) do
        print(entry)
    end
end

function listToString(l)
    local s = ""
    for _, entry in pairs(l) do
        s = s .. tostring(entry) .. "\n"
    end
    return s
end
