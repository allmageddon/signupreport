SavedSnaps = {}

function VoidSnapshotter(fname)
    local pre_wrapped = _G[fname]
    _G[fname] = snapshot_wrap(fname)
    _G[fname]()
    _G[fname] = pre_wrapped
end

function IndexTilNilSnapshotter(start)
    function snapper(fname)
        local pre_wrapped = _G[fname]
        _G[fname] = snapshot_wrap(fname)
        local i = start
        while true do
            local result = _G[fname](i)
            if result == nil then
                break
            end
            i = i + 1
        end
        _G[fname] = pre_wrapped
    end

    return snapper
end

function MonitorUsage(fname)
    _G[fname] = snapshot_wrap(fname)
end

function run_snapshots(snaps)
    for fname, snapshotter in pairs(snaps) do
        snapshotter(fname)
    end
end

--local function serialize(value)
--    if type(value) ~= 'table' then return tostring(value) end
--    keyvals = {}
--    for k, va in pairs(value) do
--        local s = serialize(k) .. "=" .. serialize(va)
--        keyvals[#keyvals + 1] = s
--    end
--    return str_join(",", keyvals)
--end
function serialize(o)
    local buffer = {}
    if type(o) == "number" then
        buffer[#buffer + 1] = o
    elseif type(o) == "nil" then
        buffer[#buffer + 1] = tostring(o)
    elseif type(o) == "boolean" then
        buffer[#buffer + 1] = tostring(o)
    elseif type(o) == "string" then
        buffer[#buffer + 1] = string.format("%q", o)
    elseif type(o) == "table" then
        buffer[#buffer + 1] = "{\n"
        for k, v in pairs(o) do
            buffer[#buffer + 1] = " " .. serialize(k) .. " = "
            buffer[#buffer + 1] = serialize(v)
            buffer[#buffer + 1] = ",\n"
        end
        buffer[#buffer + 1] = "}\n"
    else
        error("cannot serialize a " .. type(o))
    end
    return table.concat(buffer)
end

local function equivalent(a, b)
    if type(a) ~= 'table' then return a == b end
    local counta, countb = 0, 0
    for k, va in pairs(a) do
        if not equivalent(va, b[k]) then return false end
        counta = counta + 1
    end
    for _, _ in pairs(b) do countb = countb + 1 end
    return counta == countb
end

function snapshot_wrap(fname)
    local fn = _G[fname]
    local store = {} -- actual storage of values that appear to be in cache
    SavedSnaps[fname] = store
    local cache = {}
    cache = setmetatable({}, {
        __newindex = function(tbl, key, value)
            store[serialize(key)] = value
        end,
        __index = function(tbl, key)
            for k, v in pairs(store) do
                if equivalent(k, serialize(key)) then return v end
            end
        end
    })
    function cached_fn(...)
        local result = { fn(...) }
        cache[{ ... }] = result
        return unpack(result)
    end

    return cached_fn
end

function mock_API(fname, snapshot)
    setmetatable(snapshot, {
        __newindex = function(tbl, key, value)
            tbl[serialize(key)] = value
        end,
        __index = function(tbl, key)
            for k, v in pairs(snapshot) do
                if key == "nil" then
                    key = {}
                end
                local serialized = serialize(key)
                --                io.write(fname.."("..serialized..")")
                --                print(" => "..serialize(v))
                if equivalent(k, serialized) then return v end
            end
        end
    })
    _G[fname] = function(...) return unpack(snapshot[{ ... }]) end
end

function read_snapshot_file(path)
    read_snapshot(dofile(path))
end

function read_snapshot(source)
    for fname, snaps in pairs(source) do
        mock_API(fname, snaps)
    end
end

