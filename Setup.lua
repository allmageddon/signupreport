function OnLoad()
    print("SignupReport loaded.");
end

local function anomaly_handler(msg, editbox)
    local command, rest = msg:match("^(%S*)%s*(.-)$");
    -- Any leading non-whitespace is captured into command;
    -- the rest (minus leading whitespace) is captured into rest.
    if command == "report" then
        local ok, result = pcall(report, format_chat)
        if not ok then
            print("Error: " .. result)
            return nil
        end
        if rest ~= "" then
            chatLines(result, string.gsub(rest, "%s", ""))
        else
            print(result)
        end
    elseif command == "export" and not rest ~= "" then
        export_handler()
    elseif command == "snapshot" and not rest ~= "" then
        snaps = {
            GetNumGroupMembers = VoidSnapshotter,
            GetNumGuildMembers = VoidSnapshotter,
            CalendarEventGetNumInvites = VoidSnapshotter,
            GetRaidRosterInfo = IndexTilNilSnapshotter(1),
            GetGuildRosterInfo = IndexTilNilSnapshotter(1),
            CalendarEventGetInvite = IndexTilNilSnapshotter(1),
        }
        run_snapshots(snaps)
    elseif command == "test" and not rest ~= "" then
    elseif command == "signed" and not rest ~= "" then
        print(Set.new(GetAllSignups()), ", ")
    elseif command == "raid" and not rest ~= "" then
        print(Set.new(GetAllRaidMembers()), ", ")
    elseif command == "guild" and not rest ~= "" then
        print(Set.basenames(Set.new(GetAllGuildMembers())), ", ")
        -- Handle removing of the contents of rest... to something.
    else
        -- If not handled above, display some sort of help message
        print("Syntax: /anomaly (report [CHANNEL]|export)");
    end
end

SLASH_ANOMALY1, SLASH_ANOMALY2 = '/anomaly', '/asdf';
SlashCmdList["ANOMALY"] = anomaly_handler;
